#include <stdio.h>
#include "permuteur.h"
#include "tools.h" // pour la fonction random(min,max)
#include <time.h> // pour l'initialisation du random

//constructeur du permuteur
Permuteur::Permuteur(int max)
{
  //initialisation du temps
  srand(time(NULL));
  this->max = max;
  indices = new int[this->max];
  perm = nullptr;
  // remplissage du tableau d'indice
  for(int i = 0; i < this->max ; ++i){
    indices[i] = i;
  }
  //generation de la permutation
  genererPermutation();
}

//fonction pour se deplacer dans la permutation
int Permuteur::suivant()
{
  int valeur = perm[i_perm];
  i_perm += 1;
  if(i_perm >= max){
    //si fin de la permutation, generation d'une nouvelle
    genererPermutation();
    i_perm = 0;
  }
  return valeur;
}

//fonction pour generer une permutation
void Permuteur::genererPermutation(){
  if(perm != nullptr){
    delete perm;
  }
  perm = new int[max];
  //init tableau a -1
  for (int i = 0; i < max; ++i){
    perm[i] = -1;
  }

  //remplissage de la permutation avec nb aleatoires
  for (int i = 0; i < max; ++i){
    bool stop = false;
    while(!stop){
      //nombre random
      int random = randomIntervalle(0,max-1);
      if(perm[random] == -1){
          perm[random] = indices[i] ;
          stop = true;
      }
    }
  }
}

//destructeur
Permuteur::~Permuteur()
{
}
