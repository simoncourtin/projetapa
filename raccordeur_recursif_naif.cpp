
#include <iostream>

#include "raccordeur_recursif_naif.h"
#include "tools.h"

using namespace std;

int RaccordeurRecursifNaif::calculerRaccord(MatInt2* distances, int* coupe)
{
  int hauteur = distances->nLignes();
  int largeur = distances->nColonnes();
  int cost = 0;
    
    for(int i = 0 ; i < hauteur ; ++i)
        for(int j = 0 ; j < largeur; ++j){
            cout << "ligne " << i << " colonne : "<<j<<endl;
            cost+=coupe_optimale(distances,i,j);
        }

  return cost;
}
int RaccordeurRecursifNaif::coupe_optimale(MatInt2* distances, int i, int j){
    //cout << "ligne " << i << " colonne : "<<j<<endl;
    int cout = distances->get(i,j);
    if(i != 0){
        if(j == 0){
            cout+=min_double2(coupe_optimale(distances,i-1,j),
                                coupe_optimale(distances,i-1,j+1));
        }
        else if(j == distances->nColonnes()-1){
            cout+=min_double2(coupe_optimale(distances,i-1,j-1),
                                coupe_optimale(distances,i-1,j));
        }
        else{
            cout+=min_double3(coupe_optimale(distances,i-1,j-1),
                                coupe_optimale(distances,i-1,j),
                                coupe_optimale(distances,i-1,j+1));
        }
    }
    return cout;
}

RaccordeurRecursifNaif::~RaccordeurRecursifNaif()
{
  // pas de ressources a liberer
}
